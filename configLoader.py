import json


# TODO 配置文件持久化
class ConfigLoader:
    def __init__(self, filename):
        self.filename = filename
        self.config = None

    def load(self):
        with open(self.filename, 'r') as f:
            self.config = json.load(f)

    def get_config(self):
        if self.config is None:
            self.load()
        return self.config

    def save(self, config):
        with open(self.filename, 'w') as f:
            json.dump(config, f, indent=4)
